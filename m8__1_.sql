-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2023 at 08:20 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `m8`
--

-- --------------------------------------------------------

--
-- Table structure for table `appmodules`
--

CREATE TABLE `appmodules` (
  `Mid` int(11) NOT NULL,
  `Modulename` varchar(255) NOT NULL,
  `Status` enum('Y','N') NOT NULL,
  `DateCreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `appmodules`
--

INSERT INTO `appmodules` (`Mid`, `Modulename`, `Status`, `DateCreated`) VALUES
(1, 'Dashboard', 'Y', '2023-09-29 09:33:27'),
(3, 'Reports', 'Y', '2023-09-29 09:34:00'),
(4, 'Permissions', 'Y', '2023-09-29 09:34:00'),
(5, 'Sales', 'Y', '2023-09-29 09:34:34'),
(6, 'Purchase', 'Y', '2023-09-29 09:34:34'),
(7, 'Users', 'Y', '2023-09-29 10:06:48'),
(8, 'User Roles', 'Y', '2023-09-29 18:46:31'),
(10, 'New_Module', 'Y', '2023-10-04 16:07:02');

-- --------------------------------------------------------

--
-- Table structure for table `appusergrouppermissions`
--

CREATE TABLE `appusergrouppermissions` (
  `Pid` int(11) NOT NULL,
  `Moduleid` int(25) NOT NULL,
  `Gid` int(25) NOT NULL,
  `IsAllowed` enum('Y','N') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `appusergrouppermissions`
--

INSERT INTO `appusergrouppermissions` (`Pid`, `Moduleid`, `Gid`, `IsAllowed`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 'Y', '2023-09-29 12:56:25', '2023-09-29 18:26:25'),
(3, 7, 2, 'Y', '2023-09-29 13:02:41', '2023-09-29 18:32:41'),
(5, 5, 2, 'Y', '2023-09-29 13:07:04', '2023-09-29 18:37:04'),
(7, 6, 2, 'Y', '2023-09-29 13:14:17', '2023-09-29 18:44:17'),
(9, 1, 2, 'Y', '2023-09-30 19:42:08', '2023-10-01 01:12:08'),
(10, 3, 4, 'Y', '2023-10-01 01:52:50', '2023-10-01 07:22:50');

-- --------------------------------------------------------

--
-- Table structure for table `appusergroups`
--

CREATE TABLE `appusergroups` (
  `Gid` int(11) NOT NULL,
  `Groupname` varchar(255) NOT NULL,
  `Status` enum('Y','N') NOT NULL,
  `DateCreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `appusergroups`
--

INSERT INTO `appusergroups` (`Gid`, `Groupname`, `Status`, `DateCreated`) VALUES
(1, 'Super Admin', 'Y', '2023-09-29 09:30:32'),
(2, 'Admin', 'Y', '2023-09-29 09:30:32'),
(3, 'Sub-Admin', 'Y', '2023-09-29 10:11:46'),
(4, 'Accountant', 'Y', '2023-09-29 09:31:11'),
(6, 'New_Group', 'Y', '2023-10-04 16:44:56');

-- --------------------------------------------------------

--
-- Table structure for table `appusers`
--

CREATE TABLE `appusers` (
  `Uid` int(11) NOT NULL,
  `Profile` varchar(255) DEFAULT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Usergroup` int(25) NOT NULL,
  `Status` enum('Y','N') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `appusers`
--

INSERT INTO `appusers` (`Uid`, `Profile`, `Email`, `Password`, `Username`, `Usergroup`, `Status`, `created_at`, `updated_at`) VALUES
(1, '/assets/images/users/1696145009.jpg', 'mailtoosatiz@gmail.com', 'Xvc/fXINi7F1CjpG4usT5rZJqtxwXHHNKZVYgK6hRKc=', 'satiz', 1, 'Y', '2023-10-01 07:23:29', '2023-10-01 01:53:29'),
(6, '', 'admin@m8.com', 'CBGh9HnZDPFv/4WiYxxH0Q==', 'ADMIN', 2, 'Y', '2023-09-29 13:30:18', '2023-09-29 13:30:18'),
(7, '', 'test@mail.com', 'Znas38AipYcJXBXjzSSD5w==', 'dddsd', 3, 'Y', '2023-09-30 19:43:07', '2023-09-30 19:43:07'),
(8, NULL, 'mail@mail.com', '29fd785861238a2ea86c712e15c16ab6', 'sathish', 2, 'Y', '2023-10-04 15:41:09', '2023-10-04 15:41:09'),
(9, NULL, 'maiwl@mail.com', 'ec8a2fd4f17d9d4738c09b81f393fd8c', 'sathish', 2, 'Y', '2023-10-04 15:50:28', '2023-10-04 15:50:28');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appmodules`
--
ALTER TABLE `appmodules`
  ADD PRIMARY KEY (`Mid`);

--
-- Indexes for table `appusergrouppermissions`
--
ALTER TABLE `appusergrouppermissions`
  ADD PRIMARY KEY (`Pid`);

--
-- Indexes for table `appusergroups`
--
ALTER TABLE `appusergroups`
  ADD PRIMARY KEY (`Gid`);

--
-- Indexes for table `appusers`
--
ALTER TABLE `appusers`
  ADD PRIMARY KEY (`Uid`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appmodules`
--
ALTER TABLE `appmodules`
  MODIFY `Mid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `appusergrouppermissions`
--
ALTER TABLE `appusergrouppermissions`
  MODIFY `Pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `appusergroups`
--
ALTER TABLE `appusergroups`
  MODIFY `Gid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `appusers`
--
ALTER TABLE `appusers`
  MODIFY `Uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
